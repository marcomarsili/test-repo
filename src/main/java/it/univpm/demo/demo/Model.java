package it.univpm.demo.demo;

public class Model{
    public String name;
    public String surname;

    public Model(){
        this.name = null;
        this.surname = null;
    }

    public Model(String name, String surname){
        this.name = name;
        this.surname = surname;
    }

    public String getName(){
        return this.name;
    }
    public String getSurname(){
        return this.surname;
    }
}