package it.univpm.demo.demo;

import java.util.ArrayList;
 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
 
@RestController
@RequestMapping(path = "/test")
public class DemoController 
{
    private ArrayList<Model> entities = new ArrayList<Model>() ;

    @GetMapping(path="/", produces = "application/json")
    public ArrayList<Model> getEntities() 
    {
        return entities;
    }
     
    @PostMapping(path= "/", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> addEntity(@RequestBody Model entity)     
    {
        entities.add(entity);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(path="/health", produces = "application/json")
    public ResponseEntity<String> getHealthyCheck() 
    {
        String msg = "hello world!!!";
        return new ResponseEntity<String>(msg, HttpStatus.OK);
    }
}