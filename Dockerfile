FROM openjdk:11.0.5-jre-stretch
RUN mkdir /opt/app
COPY target/demo-0.0.1-SNAPSHOT.jar /opt/app
CMD ["java", "-jar", "/opt/app/demo-0.0.1-SNAPSHOT.jar"]